import re  # for using regex


def string_match_regex(string, regex):  # check if string characters match regular expression
    return bool(re.match(regex, string))


def split_email(email):  # split email into username, websitename and extension
    username = email.split('@')[0]
    websitename = email.split('@')[1].split('.')[0]
    extension = email.split('@')[1].split('.')[1]
    return username, websitename, extension


def email_validation(email, total_length_min,
                     username_allowed, websitename_allowed, extension_allowed,
                     extension_length_max):
    # this function returns boolean validation result and short description in case of error
    if len(email) < total_length_min or email.count('@') != 1 or email.count('.') != 1:
        return False, "Validation failed - it must have the username@websitename.extension format type"

    username, websitename, extension = split_email(email)

    if not string_match_regex(username, username_allowed):
        return False, "Validation failed - the username can only contain letters, digits, dashes and underscores"

    if not string_match_regex(websitename, websitename_allowed):
        return False, "Validation failed - the website name can only contain letters and digits"

    if not string_match_regex(extension, extension_allowed):
        return False, "Validation failed - the extension can only contain letters"

    if len(extension) > extension_length_max:
        return False, "Validation failed - the maximum length of the extension is 3"

    return True, "Validation passed - everything looks good"


def main():  # main function
    """initial parameters, can be imported from the file though"""
    total_length_min = 5  # email must contain at least 5 symbols to match the format
    username_allowed = "^[A-Za-z0-9_-]+$"  # regex for letters, numbers, dash and underscore
    websitename_allowed = "^[A-Za-z0-9]+$"  # regex for letters and numbers
    extension_allowed = "^[A-Za-z]+$"  # regex for letters only
    extension_length_max = 3

    email = str(input("Enter an email:\n"))  # input from the keyboard - can be from the file as well

    result, description = email_validation(email, total_length_min, username_allowed, websitename_allowed,
                                           extension_allowed, extension_length_max)
    print(description)  # print validation result to the console, can be written to the file if needed


if __name__ == '__main__':
    main()
