import re
from flask import Flask, render_template, request
import os

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/validation', methods=['POST'])
def validation():
    email = request.form['email']
    result = email_validation(email)
    return render_template("index.html", result=result, email=email)


def string_match_regex(string, regex):
    return bool(re.match(regex, string))


def split_email(email):
    username = email.split('@')[0]
    websitename = email.split('@')[1].split('.')[0]
    extension = email.split('@')[1].split('.')[1]
    return username, websitename, extension


def email_validation(email):
    total_length_min = 5
    username_allowed = "^[A-Za-z0-9_-]+$"
    websitename_allowed = "^[A-Za-z0-9]+$"
    extension_allowed = "^[A-Za-z]+$"
    extension_length_max = 3

    if len(email) < total_length_min or email.count('@') != 1 or email.count('.') != 1:
        return "Validation failed - it must have the username@websitename.extension format type"

    username, websitename, extension = split_email(email)

    if not string_match_regex(username, username_allowed):
        return "Validation failed - the username can only contain letters, digits, dashes and underscores"

    if not string_match_regex(websitename, websitename_allowed):
        return "Validation failed - the website name can only contain letters and digits"

    if not string_match_regex(extension, extension_allowed):
        return "Validation failed - the extension can only contain letters"

    if len(extension) > extension_length_max:
        return "Validation failed - the maximum length of the extension is 3"

    return "Validation passed - everything looks good"


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
